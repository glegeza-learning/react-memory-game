import React from 'react';
import PropTypes from 'prop-types';
import './Card.css';

const Card = ({size, margin, id, callback, shown, val, color}) => {
    const style = {
        maxWidth: `${size}px`,
        minWidth: `${size}px`,
        maxHeight: `${size}px`,
        minHeight: `${size}px`,
        lineHeight: `${size}px`,
        margin: `${margin}px`,
        backgroundColor: shown ? color : 'lightgrey',
    };
    return (
        <div
            className='game-card'
            style={style}
            onClick={() => callback(id)}>
            <p>{shown ? val : '?'}</p>
        </div>
    );
};

Card.propTypes = {
    size: PropTypes.number.isRequired,
    margin: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    callback: PropTypes.func.isRequired,
    shown: PropTypes.bool.isRequired,
    val: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
};

Card.defaultProps = {
    size: 100,
    margin: 4,
    callback: (id) => {},
    shown: false,
};

export default Card;
