import React, {Component} from 'react';
import './App.css';
import PropTypes from 'prop-types';
import Game from './Game.js';
import NavBar from './NavBar.js';

const randomcolor = require('randomcolor');

class App extends Component {
  constructor(props) {
    super(props);
    const {cardsPerRow, rows} = this.props;
    this.state = {
      cards: this.getCardStates(cardsPerRow * rows),
      flipped: [],
      waiting: false,
      cardsLeft: cardsPerRow * rows,
    };
    this.onCardClicked = this.onCardClicked.bind(this);
    this.resetGame = this.resetGame.bind(this);
  }

  resetGame() {
    const {cardsPerRow, rows} = this.props;
    const cards = this.getCardStates(cardsPerRow * rows);
    const flipped = [];
    const waiting = false;
    const cardsLeft = cardsPerRow * rows;
    this.setState({cards, flipped, waiting, cardsLeft});
  }

  onCardClicked(id) {
    if (this.state.waiting ||
        this.state.cards[id].isRevealed ||
        this.state.flipped.includes(id)) {
            return;
        }
    let {flipped, cards, cardsLeft} = this.state;
    flipped = [...flipped, id];
    cards = this.state.cards.map((c) => {
        if (c.id === id) {
            return {
                id: c.id,
                isFlipped: true,
                isRevealed: c.isRevealed,
                val: c.val,
            };
        }
        return c;
    });

    let waiting = flipped.length === 2;
    if (waiting) {
        if (cards[flipped[0]].val.number === cards[flipped[1]].val.number) {
            cards[flipped[0]].isRevealed = true;
            cards[flipped[1]].isRevealed = true;
            cards[flipped[0]].isFlipped = false;
            cards[flipped[1]].isFlipped = false;
            flipped = [];
            waiting = false;
            cardsLeft -= 2;
        } else {
            setTimeout(() => {
                this.clearFlipped();
            }, 1000);
        }
    }
    this.setState({flipped, cards, waiting, cardsLeft});

    if (cardsLeft === 0) {
        setTimeout(() => {
            this.resetGame();
        }, 1000);
    }
  }

  getCardStates(numCards) {
    let cards = [];
    let numbers = [];
    let colors = randomcolor({count: numCards, luminosity: 'bright'});
    for (let i = 0; i < numCards / 2; i++) {
        let val = {
            number: String(i + 1),
            color: colors[i],
        };
        numbers.push(val);
        numbers.push(val);
    }
    let remaining = numCards;
    for (let i = 0; i < numCards; i++) {
        let idx = Math.floor(Math.random() * remaining--);
        let selected = numbers[idx];
        numbers[idx] = numbers[remaining];
        numbers[remaining] = selected;
        cards.push({
            id: i,
            isFlipped: false,
            isRevealed: false,
            val: selected,
        });
    }
    return cards;
  };

  clearFlipped() {
    console.log('clearing');
    let cards = this.state.cards.map((c) => {
        return {
            id: c.id,
            isFlipped: false,
            isRevealed: c.isRevealed,
            val: c.val,
        };
    });
    let flipped = [];
    let waiting = false;
    this.setState({flipped, waiting, cards});
  }

  render() {
    return (
      <div className="App">
        <NavBar reset={this.resetGame} />
        <Game
          cards={this.state.cards}
          clickCallback={this.onCardClicked}
          {...this.props} />
      </div>
    );
  }
}

App.defaultProps = {
  rows: 4,
  size: 100,
  cardsPerRow: 6,
  margin: 4,
  padding: 20,
};

App.propTypes = {
  rows: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  cardsPerRow: PropTypes.number.isRequired,
  margin: PropTypes.number.isRequired,
  padding: PropTypes.number.isRequired,
};

export default App;
