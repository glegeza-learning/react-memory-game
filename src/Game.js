import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Card from './Card.js';
import './Game.css';

class Game extends Component {
    render() {
        const {
            size,
            margin,
            cardsPerRow,
            padding,
            clickCallback} = this.props;
        const boxSize = size + margin * 2;
        const style = {
            width: `${cardsPerRow * boxSize}px`,
            padding: `${padding}px`,
        };
        const cards = this.getCards(
            this.props.cards, size, margin, clickCallback);

        return (
            <div className='game-container' style={style}>
                {cards}
            </div>
        );
    }

    getCards(cards, size, margin, callback) {
        return cards.map((c) => {
            return <Card
                    key={c.id}
                    id={c.id}
                    shown={c.isFlipped || c.isRevealed}
                    size={size}
                    margin={margin}
                    val={c.val.number}
                    color={c.val.color}
                    callback={callback} />;
        });
    }
}

Game.defaultProps = {
    rows: 4,
    size: 100,
    cardsPerRow: 6,
    margin: 4,
    padding: 20,
    clickCallback: (id) => {},
};

Game.propTypes = {
    rows: PropTypes.number.isRequired,
    size: PropTypes.number.isRequired,
    cardsPerRow: PropTypes.number.isRequired,
    margin: PropTypes.number.isRequired,
    padding: PropTypes.number.isRequired,
    cards: PropTypes.arrayOf(PropTypes.object).isRequired,
    clickCallback: PropTypes.func.isRequired,
};

export default Game;
