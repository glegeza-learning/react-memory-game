module.exports = {
    "env" : {
        "es6": true,
        "browser": true,
        "commonjs": true,
        "mocha": true,
        "node": false,
    },
    "rules" : {
        "linebreak-style": 0,
        "require-jsdoc": 0,
        "no-unused-vars": 0
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2017,
		"sourceType": "module",
    },
    "plugins" : [
        "react",
        "babel"
    ],
    "extends": [
        "google",
        "plugin:react/recommended",
    ],
};