import React from 'react';
import PropTypes from 'prop-types';
import './NavBar.css';

const NavBar = (props) => {
    return (
        <div className='nav-bar'>
            <h1>Memory Game</h1>
            <a onClick={props.reset}>New Game</a>
        </div>
    );
};

NavBar.propTypes = {
    reset: PropTypes.func.isRequired,
};

export default NavBar;
